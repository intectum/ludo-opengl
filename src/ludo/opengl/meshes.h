/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_OPENGL_GEOMETRY_H
#define LUDO_OPENGL_GEOMETRY_H

#include <ludo/meshes.h>

namespace ludo
{
  LUDO_API void bind(const mesh_buffer& mesh_buffer);
}

#endif // LUDO_OPENGL_GEOMETRY_H
