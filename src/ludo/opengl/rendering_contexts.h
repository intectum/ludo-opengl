/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_OPENGL_RENDERING_CONTEXTS_H
#define LUDO_OPENGL_RENDERING_CONTEXTS_H

#include <ludo/rendering.h>

namespace ludo
{
  LUDO_API void bind(const rendering_context& rendering_context);
}

#endif // LUDO_OPENGL_RENDERING_CONTEXTS_H
