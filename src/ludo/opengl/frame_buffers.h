/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_OPENGL_FRAME_BUFFERS_H
#define LUDO_OPENGL_FRAME_BUFFERS_H

#include <ludo/rendering.h>

namespace ludo
{
  LUDO_API void bind(const frame_buffer& frame_buffer);
}

#endif // LUDO_OPENGL_FRAME_BUFFERS_H
