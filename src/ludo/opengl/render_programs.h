/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_OPENGL_RENDER_PROGRAMS_H
#define LUDO_OPENGL_RENDER_PROGRAMS_H

#include <ludo/rendering.h>

namespace ludo
{
  LUDO_API void bind(const render_program& render_program);
}

#endif // LUDO_OPENGL_RENDER_PROGRAMS_H
